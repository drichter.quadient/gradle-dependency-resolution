plugins {
    `java-library`
    `maven-publish`
}

group = "public.lib"
version = "1.0.0"

repositories {
    mavenCentral()
}

dependencies {
    api("org.jetbrains.kotlin:kotlin-test-junit:1.5.21")
    implementation("org.slf4j:slf4j-api:1.7.30")
}

publishing {
    publications {
        val lib by creating(MavenPublication::class) {
            from(components["java"])
        }
    }
    repositories {
        maven {
            url = rootDir.resolve("repo").toURI()
        }
    }
}
