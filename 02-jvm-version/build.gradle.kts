plugins {
    `java-library`
}

group = "gradle.dependency.resolution"
version = "1.0.0-SNAPSHOT"

repositories {
    maven {
        url = rootDir.resolve("repo").toURI()
    }
    mavenCentral()
}
java {
    toolchain {
//        languageVersion.set(JavaLanguageVersion.of(8))
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}


dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.1")
    testImplementation("public.lib:publicly-published-library:1.0.0")
}

tasks.test {
    // Use JUnit Platform for unit tests.
    useJUnitPlatform()
}

