rootProject.name = "gradle-dependency-resolution"
include("01-basics")
include("02-jvm-version")
include("03-capability-conflict")
include("04-missing-capabilities")
include("05-versions-handling")
include("publicly-published-library")


dependencyResolutionManagement {
    repositories {
        maven {
            url = rootDir.resolve("repo").toURI()
        }
        mavenCentral()
    }
}