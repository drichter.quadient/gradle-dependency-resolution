plugins {
    `java-library`
}

group = "gradle.dependency.resolution"
version = "1.0.0-SNAPSHOT"


dependencies {
    components.all(LoggingCapability::class.java)
    testImplementation("log4j:log4j:1.2.17")
    testImplementation("org.slf4j:slf4j-simple:1.7.31")
//    testImplementation("org.slf4j:slf4j-log4j12:1.7.31")
    testImplementation("org.slf4j:log4j-over-slf4j:1.7.31")
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.1")
}

tasks.test {
    // Use JUnit Platform for unit tests.
    useJUnitPlatform()
}

class LoggingCapability: ComponentMetadataRule {
    override fun execute(ctx: ComponentMetadataContext) {
        if (ctx.details.id.name.contains("log4j")) {
            ctx.details.allVariants {
                withCapabilities {
                    addCapability("org.slf4j", "slf4j-log4j-capability", ctx.details.id.version)
                }
            }
        }
    }
}