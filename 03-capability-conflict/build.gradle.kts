plugins {
    `java-library`
}

group = "gradle.dependency.resolution"
version = "1.0.0-SNAPSHOT"

repositories {
    maven {
        url = rootDir.resolve("repo").toURI()
    }
    mavenCentral()
}

//configurations.all {
//    resolutionStrategy.capabilitiesResolution.withCapability("org.jetbrains.kotlin:kotlin-test-framework-impl") {
//        select(candidates.first { it.id.displayName.contains("junit5") })
//        because("I use junit 5")
//    }
//}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.1")
    testImplementation("org.jetbrains.kotlin:kotlin-test:1.5.21")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5:1.5.21")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:1.5.21")
//    testImplementation("public.lib:publicly-published-library:1.0.0")
}

tasks.test {
    // Use JUnit Platform for unit tests.
    useJUnitPlatform()
}

