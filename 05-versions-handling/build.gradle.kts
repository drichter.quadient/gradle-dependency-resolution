plugins {
    `java-library`
}

dependencies {
    constraints {
        testImplementation("org.bouncycastle:bcprov-jdk15on") {
            version {
                strictly("1.63")
            }
            because("bouncy castle 1.65 contains vulnerability CVE-666")
        }
    }
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.1")
    testImplementation("cz.d1x:dxcrypto-bc:2.4.0")
    testImplementation("org.slf4j:slf4j-api") {
        version {
            require("[1.7.20,2.0[")
            prefer("1.7.31")
        }
    }
    testImplementation("public.lib:publicly-published-library:1.0.0")
}